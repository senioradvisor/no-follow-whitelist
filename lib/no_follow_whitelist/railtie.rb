require "rails"

module NoFollowWhitelist
  class Railtie < ::Rails::Railtie
  
    config.before_configuration do
      Rails.application.config.middleware.use Rack::NoFollowWhitelist
    end
    
    private
    
  end
end