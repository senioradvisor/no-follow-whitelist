
module Rack
  
  class NoFollowWhitelist
    
    def initialize(app)
      @app=app
    end

    def call(env)
      @status, @headers, @response = @app.call(env)
      @request = Rack::Request.new(env)
      [@status, _apply_headers, @response]
    end

    private

      def _apply_headers
        if _match_request_url?(@request.url)
          @headers['X-Robots-Tag'] = 'noindex, nofollow'
        end
        @headers
      end
    
      def _match_request_url?(url)
        w_regex =  ENV['NO_FOLLOW_WHITELIST_REGEX']
        b_regex = ENV['NO_FOLLOW_BLACKLIST_REGEX']
        return true if w_regex.present? && !(/#{w_regex}/ =~ url)
        return true if b_regex.present? && /#{b_regex}/ =~ url
        false
      end
      
  end
  
end
