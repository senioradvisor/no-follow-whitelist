# No Follow Whitelist

Based on Yarin Kessler's hide-heroku gem, no-follow-whitelist hides all non whitelisted URLs if the environment variable NO_FOLLOW_WHITELIST is set, from search engines in order to avoid duplicate content issues.

## Info

No-Follow-Whitelist blocks robots from all content served under the non whitelisted domains, *including* assets, by issuing 'noindex, nofollow' **X-Robots-Tag** HTTP response headers on those requests.

More on **X-Robots-Tags**:

 - [Robots meta tag and X-Robots-Tag HTTP header specifications - Google Webmasters](https://developers.google.com/webmasters/control-crawl-index/docs/robots_meta_tag)
 - [Preventing your site from being indexed, the right way • Yoast](http://yoast.com/prevent-site-being-indexed/)
 - [Using the X-Robots-Tag HTTP Header Specifications in SEO: Tips and Tricks - SEO Chat](http://www.seochat.com/c/a/search-engine-optimization-help/using-the-x-robots-tag-http-header-specifications-in-seo-tips-and-tricks/)


## Installation

no_follow_whitelist is Rack middleware that can be used with Rails or any other Rack application.

There are two environment variables that this gem check

NO_FOLLOW_WHITELIST_REGEX could be a complex regex to target certain patterns that will be allowed to be indexed.

NO_FOLLOW_BLACKLIST_REGEX could be a complex regex to target certain patterns that won't be allowed to be indexed.


Add this line to your application's Gemfile:

    gem 'no_follow_whitelist'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install no_follow_whitelist

For non-rails applications, add this line to the config.ru file:

    use Rack::NoFollowWhitelist


