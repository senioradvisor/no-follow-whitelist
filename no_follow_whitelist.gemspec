# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name          = "no_follow_whitelist"
  spec.version       = "0.0.1"
  spec.authors       = ["Elias Lopez Gutierrez"]
  spec.email         = ["eliaslopez@hotmail.com"]
  spec.description   = "Hides non whitelisted URLs from search engines in order to avoid duplicate content issues"
  spec.summary       = "Hides non whitelisted URLs from search engines"
  spec.homepage      = "https://github.com/eliaslopezgt/no-follow-whitelist"
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
